<?php
// $Id$

/**
 * @file
 * Feeds YouTube API fetcher class
 */

/**
 * Class definition for YouTube API fetcher
 *
 * Retrieves data retrieved using the YouTube API.
 * Handles YouTube "watch URLs" and "user URLs.
 * @TODO: Support "AJAX" URL's on format http://www.youtube.com/user/USERNAME#p/a/u/1/VIDEO_ID and treat those as a single video URL's
 */
class FeedsYouTubeApiFetcher extends FeedsFetcher {
  
  const YOUTTUBE_URL_WATCH = 'www.youtube.com/watch';
  const YOUTTUBE_URL_USER = 'www.youtube.com/user';

  /**
   * Implements FeedsFetcher::fetch().
   * Object containing fetch method and fetch argument (username/videoID)
   *
   * @param string $url
   * @return FeedsYoutubeFetcherResult
   * @throws Exception Throws an exception if provided URL can not be parsed.
   */
  public function fetch(FeedsSource $source) {
    $source_config	= $source->getConfigFor($this);
    $url = isset($source_config['url']) ? $source_config['url'] : ''; 
    $fetch_method = '';
    $argument = '';
    
    $argument = $this->getUser($url);
    if($argument) {
    	$fetch_method = FeedsYoutubeFetcherResult::FETCH_METHOD_USER_VIDEOS;    	
    }
    
    if(!$fetch_method) {
    	$argument = $this->getVideoId($url);
    	$fetch_method = FeedsYoutubeFetcherResult::FETCH_METHOD_SINGLE_VIDEO;
    }
    
    if(!$argument || !$fetch_method) {
    	throw new Exception(t('Could not create fetcher result object. URL might be invalid: @url.', array('@url' => $url)), E_WARNING);    	
    }
    
		return new FeedsYoutubeFetcherResult($argument, $fetch_method);    
  } 
  
  /**
   * Expose source form.
   * 
   * @return sourceForm.
   */
  public function sourceForm($source_config) {
    $form = array();
        
    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => t('Youtube Url'),
      '#description' => t('The URL to a YouTube user or single video page. Following URL\'s are supported:<ul><li>@user_url/USERNAME</li><li>@single_video_url?v=VIDEO_ID</li></ul>', array('@user_url' => self::YOUTTUBE_URL_USER, '@single_video_url' => self::YOUTTUBE_URL_WATCH)),
      '#default_value' => isset($source_config['url']) ? $source_config['url'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );
    
    return $form;
  }
  
  /**
   * Parses and returns the video ID from the given URL
   * Valid format: http://www.youtube.com/watch?v=[videoID]...
   * 
   * @param string $url
   * @return string $video_id
   */
  protected function getVideoId($url) {
  	$video_id = '';
  	 	 
    if(strpos($url, self::YOUTTUBE_URL_WATCH) !==false)
    {
    	$query = parse_url($url, PHP_URL_QUERY);			
			$query_array = preg_split("/[&=]/", $query);
			
			for ($i = 0; $i < count($query_array); $i++) {
				if($query_array[$i] == "v") {
					$video_id = isset($query_array[$i+1]) ? $query_array[$i+1] : '';
					break;
				}
			}
    }
    
  	return $video_id;
  }
  
  /**
   * Parses and returns the username from the given URL
   * Valid format: http://www.youtube.com/user/[username]?...
   * 
   * @param string $url
   * @return string $username
   */
	protected function getUser($url) {
  	$user = '';
  	$parsed_url	= parse_url($url);
  	$host = isset($parsed_url['host']) ? $parsed_url['host'] : '';
  	
  	if(strpos($url, self::YOUTTUBE_URL_USER)) {
	  	$path_array	= isset($parsed_url['path']) ? explode('/', $parsed_url['path']) : array();
	  	
	  	foreach($path_array as $index => $path) {
	  		if($path == 'user') {
	  			$user = isset($path_array[$index+1]) ? $path_array[$index+1] : '';
	  			break;
	  		}
	  	}
  	}
  	
  	return $user;
  }
  
  /**
   * Validates the given URL and checks if it is supported.
   * Check FeedsYouTubeApiFetcher for valid types
   */
  public function sourceFormValidate(&$source_config) {
  	  	
  	$is_valid = false;
  	$url = isset($source_config['url']) ? trim($source_config['url']) : '';
  	$source_config['url'] = $url;	// Make sure the URL is trimmed
  	  	
  	// If http is missing we add it
  	if($url && strpos($url, "http") !== 0) {
  		$url = 'http://' . $url;
  		$source_config['url'] = $url;
  	}
  	
  	if($url) {
			if($this->getUser($url) != '') {
				$is_valid = true;
			}
			else {
				$is_valid = $this->getVideoId($url) != '';
			}
  	}
  	
  	if(!$is_valid) {
			form_set_error('feeds][url', t('The URL you have entered is not valid. It should be of form @watch_url or @user_url.', array('@user_url' => self::YOUTTUBE_URL_USER.'/USERNAME', '@watch_url' => self::YOUTTUBE_URL_WATCH.'?v=VIDEO_ID')));  		
  	}  	
  }
  
}
