<?php
// $Id$

/**
 * @file
 * Feeds YouTube API fetcher result class
 */

/**
 * Feeds YouTube API Fetcher result class
 * Handles fetch methods for a single video or a user feed.
 * If valid arguments is provided a "raw" YouTube feed is returned as result
 */
class FeedsYouTubeApiFetcherResult extends FeedsFetcherResult {

  const FETCH_METHOD_USER_VIDEOS = 'user_videos';
  const FETCH_METHOD_SINGLE_VIDEO = 'single_video';	
	
  protected $argument = '';
  protected $fetch_method = '';
	
  /**
   * Constructor.
   */
  public function __construct($argument, $fetch_method) {
  	$this->fetch_method = $fetch_method;
  	$this->argument = $argument;    
   	    
    parent::__construct('');
  }

  /**
   * Overrides FeedsFetcherResult::getRaw();
   * 
   * Retrieves data using the YouTube API.
   * All valid results will be retured as feeds even if a single entry was selected.
   * 
   * @return $string, XML response from YouTube or empty string if something was wrong 
   */
  public function getRaw() {    
    $result = '';
    
  	switch($this->fetch_method) {
  		case self::FETCH_METHOD_SINGLE_VIDEO:
  			$url = "http://gdata.youtube.com/feeds/api/videos/" . $this->argument;
  			break;
  		case self::FETCH_METHOD_USER_VIDEOS:
  			$url = "http://gdata.youtube.com/feeds/api/users/" . $this->argument . "/uploads";
  			break;
  		default:
  			$url = '';
  	}
  	
  	if($url) {
  		try {			  
  			feeds_include_library('http_request.inc', 'http_request');
			  $result = http_request_get($url);
			  $this->alterResult($result);
			  
			  if($result && isset($result->data)) {
			  	$result  = $result->data;
			  }			  
			  
			}
			catch (Exception $e) {
				watchdog('YouTube API Fetcher', 'Exception when getting URL '.$url.': '.$e->getMessage());
			}
  	}
  	else {
  		watchdog('YouTube API Fetcher', '"'.$this->fetch_method.' is no valid fetch method');
  	}
  	
    return $result;
  }
  
  /**
   * Function that can alter the result retrieved from YouTube.
   * If a atom feed parser is used to parse the data we need to make a single entry
   * look like a feed hence we wrap it with feed elements 
   * 
   * @param Object $result, The result returned using the YouTube API
   */
  protected function alterResult(&$result) {  	
  	if(isset($result->data) && $this->fetch_method == self::FETCH_METHOD_SINGLE_VIDEO) {
			$result->data = str_replace("<?xml version='1.0' encoding='UTF-8'?>","",$result->data);
			$result->data = "<?xml version='1.0' encoding='UTF-8'?><feed>" . $result->data . "</feed>";
		}
  }
  
}